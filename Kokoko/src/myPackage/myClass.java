package myPackage;

import java.util.Scanner;

public class myClass {
    public static void main(String[] args) {
        int a;
        int n;
        Scanner cin = new Scanner(System.in);
        a = cin.nextInt();
        n = cin.nextInt();
        System.out.println(pow(a, n));
    }

    public static int pow(int a, int n){
        int res = 1;
        for (int i = 0; i<n; i++){
            res = res*a;
        }
        return res;
    }

}